#ifndef ONE_ROBOCLAW_DRIVE_TRAIN_HPP
#define ONE_ROBOCLAW_DRIVE_TRAIN_HPP

#include <memory>

#include <rip/components/roboclaw.hpp>
#include <rip/datatypes/motor_dynamics.hpp>
#include <rip/dab/differential_drivetrain.hpp>
#include <units.h>

namespace rip::components
{
    /**
     * Abstract base class for the drive train
     */
    class OneRoboclawDrivetrain : public DifferentialDrivetrain
    {
        using Roboclaw = rip::components::Roboclaw;
        using MotorDynamics = rip::datatypes::MotorDynamics;
    public:
        OneRoboclawDrivetrain(std::shared_ptr<Roboclaw> rclaw);

        OneRoboclawDrivetrain(const std::string& name, const nlohmann::json& config,
            std::shared_ptr<emb::host::EmbMessenger> emb,
            std::shared_ptr<std::unordered_map<std::string, 
            std::shared_ptr<RobotComponent> > > comps);

        ~OneRoboclawDrivetrain();

        /**
         * Drive all the motors
         * @param power [-1, 1]
         */
        virtual void drive(double power) override;

        /**
         * Drive left and right separately
         * @param left [-1, 1]
         * @param right [-1, 1]
         */
        virtual void drive(double left, double right) override;

        /**
         * Single command to all motors
         */
        virtual void drive(const MotorDynamics& command) override;

        /**
         * Command left and right sides separately
         */
        virtual void drive(const MotorDynamics& left, const MotorDynamics& right) override;

        /**
         * Reads encoders for every motor you tell it to read, reports back in respective
         * order
         */
        virtual std::vector<inch_t> readEncoders() override;
        /**
         * reads the encoder for one motor
         */
        virtual inch_t readEncoder(unsigned int motor) override;
        /**
         * Reads encoder velocity for every motor you tell it to read, reports back in respective
         * order
         * @param motors list of motors to read
         */
        virtual std::vector<inches_per_second_t> readEncoderVelocities() override;
        /**
         * reads the encoder for one motor
         */
        virtual inches_per_second_t readEncoderVelocity(unsigned int motor) override;

        virtual void stop() override;
        /**
         * Set persistent motor driving profiles for speed, accel.
         */
        void setDynamics(const MotorDynamics& dynamics);

    private:
        std::shared_ptr<Roboclaw> m_rclaw;
    };
}

#endif // DRIVE_TRAIN_HPP
