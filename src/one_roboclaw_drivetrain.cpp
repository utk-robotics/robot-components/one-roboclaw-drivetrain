#include "rip/components/one_roboclaw_drivetrain.hpp"
#include <cmath>

namespace rip::components
{
    NEW_RIP_EX(OutOfRangeException)
    NEW_RIP_EX(RoboclawNotFoundException)
    NEW_RIP_EX(RoboclawIdentifierNotFoundException)

    OneRoboclawDrivetrain::OneRoboclawDrivetrain(const std::string& name,
        const nlohmann::json& config, 
        std::shared_ptr<emb::host::EmbMessenger> emb, 
        std::shared_ptr<std::unordered_map<std::string, 
        std::shared_ptr<RobotComponent> > > comps) 
        : DifferentialDrivetrain(name, config, emb, comps)
    {
        // check for mfkn roboclaw n such via config.
        auto comp_ids = config.find("roboclaw");
        if(comp_ids == config.end())
        {
            throw RoboclawIdentifierNotFoundException("No component \
            identifier found.");
        }
        std::string identifier  = comp_ids.value();
        auto rclaw_it = comps->find(identifier);
        if(rclaw_it == comps->end()) 
        {
            throw RoboclawNotFoundException("Searched for roboclaw with \
                identifier {}", identifier);
        }

    } 

    OneRoboclawDrivetrain::OneRoboclawDrivetrain(std::shared_ptr<Roboclaw> rclaw)
    : m_rclaw(rclaw)
    {
    }

    OneRoboclawDrivetrain::~OneRoboclawDrivetrain()
    {
        stop();
    }

    void OneRoboclawDrivetrain::drive(double power)
    {
        if (std::abs(power) > 1)
        {
            throw OutOfRangeException("out of range");
        }
        m_rclaw->DutyM1M2(static_cast<int16_t>(32767 * power), 
            static_cast<int16_t>(32767 * power));
    }

    void OneRoboclawDrivetrain::drive(double left, double right)
    {
        if (std::abs(left) > 1 || std::abs(right) > 1)
        {
            throw OutOfRangeException("out of range");
        }
        m_rclaw->DutyM1(32767 * left);
        m_rclaw->DutyM2(32767 * right);
    }

    void OneRoboclawDrivetrain::drive(const MotorDynamics& command)
    {
        int32_t speed;
        uint32_t accel, dist, decel;

        switch (command.getDType())
        {
            case MotorDynamics::DType::kNone:
                return;
            case MotorDynamics::DType::kSpeed:
                speed = static_cast<int32_t>(command.getSpeed());
                // TODO: Complete command implementation on roboclaw side.
                // m_rclaw->SpeedM1M2(speed);
                return;
            case MotorDynamics::DType::kSpeedAccel:
                speed = static_cast<int32_t>(command.getSpeed());
                accel = static_cast<uint32_t>(command.getAcceleration());
                // TODO: Complete command implementation on roboclaw side.
                // m_rclaw->SpeedAccelM1M2(accel, speed);
                return;
            case MotorDynamics::DType::kSpeedDist:
                speed = static_cast<int32_t>(command.getSpeed());
                dist = static_cast<uint32_t>(command.getDistance());
                // TODO: Complete command implementation on roboclaw side.
                // m_rclaw->SpeedDistanceM1M2();
                return;
            case MotorDynamics::DType::kSpeedAccelDist:
                speed = static_cast<int32_t>(command.getSpeed());
                dist = static_cast<uint32_t>(command.getDistance());
                accel = static_cast<uint32_t>(command.getAcceleration());
                // TODO: Complete command implementation on roboclaw side.
                // m_rclaw->SpeedAccelDistanceM1M2();
                return;
            case MotorDynamics::DType::kSpeedAccelDecelDist:
                speed = static_cast<int32_t>(command.getSpeed());
                dist = static_cast<uint32_t>(command.getDistance());
                accel = static_cast<uint32_t>(command.getAcceleration());
                decel = static_cast<uint32_t>(command.getDeceleration());
                // TODO: Complete command implementation on roboclaw side.
                // m_rclaw->SpeedAccelDeccelPositionM1M2();

                return;
        default:
            assert(false);
        }
    }

    void OneRoboclawDrivetrain::drive(const MotorDynamics& left, const MotorDynamics& right)
    {

    }

    void OneRoboclawDrivetrain::stop()
    {
        drive(0);
    }
}
